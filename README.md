# jmmc-python-webservices-kubernetes pyws pods

K8S config for the stateless jmmc's python webservices instance(s).

* Check the backend you want to update:
  * kubectl describe ingress pyws-blue-green-ingress
* Kustomize the right color
  * vi overlays/blue/kustomization.yml
* Apply it
  * kustomize build overlays/blue | kubectl apply -f -

